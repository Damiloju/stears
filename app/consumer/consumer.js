/* eslint no-underscore-dangle: [2, { "allow": ["_id"] }] */


const dotenv = require('dotenv');

dotenv.config();

const config = require('../config/config');

// service locator via dependency injection
const serviceLocator = require('../config/di');

const rabbitMQClient = serviceLocator.get('rabbitmq');
const rabbitQueue = config.rabbitMQ.queue;

const reportingService = require('../services/report');



rabbitMQClient.then((connection) => {
    // create a freq channel
    connection.createChannel().then((reportChannel) => {
      reportChannel.on('close', () => {
        logger.error('seFeedbackChannel closed');
        process.exit(1);
      });
    //   reportChannel.on('error', () => {
    //     logger.error('seFeedbackChannel error');
    //     process.exit(1);
    //   });
      reportChannel.prefetch(10);
      reportChannel.assertQueue(rabbitQueue, { durable: true, noAck: false })
        .then(() => reportChannel.consume(rabbitQueue, (messageObject) => {
          if (messageObject !== null) {
            const message = messageObject.content.toString();
            const msgObj = JSON.parse(message);
            logger.info('-----transforming object -------');
            return reportingService.getReportingObjects(msgObj.survey_id,msgObj.client_email)
              .then((reportingResponse) => {
                  if(reportingResponse !== null ){
                reportChannel.ack(messageObject);
                // send email with the reporting object downloadable link
                  }else{
                    logger.error(`Reporting Response Error: ${reportingResponse}`);

                  }
              })
              .catch((transformationtError) => {
                logger.error(`Reporting Service Call Error: ${transformationtError}`);
                // reportChannel.ack(messageObject);
              });
          }
          return null;
        }));
    });
  });
