

const Response = require('../lib/response_manager');
const HttpStatus = require('../constants/http_status');
const _ = require('lodash');
const moment = require('moment');
class dashboardController {
/**
     * Class Constructor
     *
     * @param dashboardService - the dashboard service (our side)
     */
    constructor(dashboardService) {
        this.dashboardService = dashboardService;
      }

    async questionAnalysisReport(req, res){
        let {survey_id} = req.query;
        if (!survey_id){
            return Response.failure(res, { message: 'Kindly add survey_id in the query' }, HttpStatus.BAD_REQUEST);
        }
        let params = {
            survey_id
        }
        
        const response = await this.dashboardService.questionAnalysisReportDetails(params);
        let defined = response.hits.hits;
        let answer = [];
        defined.forEach((obj) => {
            answer.push(obj._source);
        })     
        return Response.success(res, {
            error: false,
            message: 'Your Records were successfully fetched',
            response: answer
        }, HttpStatus.OK);
    }

   
};

module.exports = dashboardController;