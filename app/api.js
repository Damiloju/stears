/**
 * Created by Adebiyi Samuel
 */

const dotenv = require('dotenv');

dotenv.config();
const restify = require('restify');
const plugins = require('restify-plugins');
const corsMiddleware = require("restify-cors-middleware");

const config = require('../app/config/config');
const dashBoardRoutes = require('./routes/route');

// service locator via dependency injection
const serviceLocator = require('../app/config/di');

const logger = serviceLocator.get('logger');


const server = restify.createServer({
  name: config.app_name,
  versions: ['1.0.0']
});

// set API versioning and allow trailing slashes
server.pre(restify.pre.sanitizePath());


 const cors = corsMiddleware({
  preflightMaxAge: 5,
  origins: ["*"]
});

server.pre(cors.preflight);
server.use(cors.actual);

// set request handling and parsing
server.use(plugins.acceptParser(server.acceptable));
server.use(plugins.queryParser());
server.use(plugins.bodyParser());


// setup Routing and Error Event Handling
dashBoardRoutes(server, serviceLocator);


server.listen(config.api_server.port, () => {
  logger.info(`${server.name} listening at ${server.url}`);
});
