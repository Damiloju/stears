

const routes = function routes(server, serviceLocator) {
  const dashboard = serviceLocator.get('dashboardController');
  /**
     * dashboard Routes
  */

    server.get({
      path: '/',
      name: 'app health check',
      version: '1.0.0'
    }, (req, res) => res.send('Welcome to the dashboard API for interactive message'));

};


module.exports = routes;
